<?php

namespace Drupal\entity_jump_menu\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Tests the implementation of Entity jump menu toolbar integration.
 *
 * @group EntityJumpMenu
 */
class EntityJumpMenuToolbarTest extends WebTestBase {

  /**
   * An admins user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['toolbar', 'node', 'user', 'test_page_test', 'entity_jump_menu'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Create an administrative user and log it in.
    $this->adminUser = $this->drupalCreateUser(['access toolbar', 'access entity jump menu toolbar']);
    $this->drupalLogin($this->adminUser);

    // Create a node.
    $this->drupalCreateContentType(['type' => 'page']);
    $this->node = $this->drupalCreateNode();
  }

  /**
   * Tests that entity jump menu added to toolbar.
   */
  public function testEntityJumpMenuToolbar() {
    // Check that no entity id is set.
    $this->drupalGet('test-page');
    $this->assertSession()->fieldValueEquals('edit-entity-id', '');
    $this->drupalGet('test-page');

    // Check invalid entity type and id.
    $this->submitForm(['entity_type' => 'node', 'entity_id' => '999'], $this->t('Go'));
    $this->assertSession()->responseContains('<input data-drupal-selector="edit-entity-id" type="text" id="edit-entity-id" name="entity_id" value="999" size="6" maxlength="10" class="form-text required error" required="required" aria-required="true" aria-invalid="true" />');
    $this->assertSession()->responseContains('There are no entities matching "<em class="placeholder">node</em>:<em class="placeholder">999</em>".');
    $this->drupalGet('test-page');

    // Check jump to a node.
    $this->submitForm(['entity_type' => 'node', 'entity_id' => '1'], $this->t('Go'));
    $this->assertSession()->responseNotContains('There are no entities matching "<em class="placeholder">node</em>:<em class="placeholder">1</em>".');
    $this->assertSession()->responseContains('<title>' . $this->node->getTitle() . ' | Drupal</title>');

    // Check that node entity type and id is set.
    $this->drupalGet('node/' . $this->node->id());
    $this->assertTrue($this->assertSession()->optionExists('edit-entity-type', 'node')->hasAttribute('selected'));
    $this->assertSession()->fieldValueEquals('edit-entity-id', $this->node->id());
    $this->drupalGet('node/' . $this->node->id());

    // Check jump to a user.
    $this->submitForm(['entity_type' => 'user', 'entity_id' => $this->adminUser->id()], $this->t('Go'));
    $this->assertSession()->responseContains('<title>' . $this->adminUser->label() . ' | Drupal</title>');

    // Check that user entity type and id is set.
    $this->drupalGet('user/' . $this->adminUser->id());
    $this->assertTrue($this->assertSession()->optionExists('edit-entity-type', 'user')->hasAttribute('selected'));
    $this->assertSession()->fieldValueEquals('edit-entity-id', $this->adminUser->id());
  }

}
